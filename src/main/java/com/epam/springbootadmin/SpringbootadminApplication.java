package com.epam.springbootadmin;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import de.codecentric.boot.admin.server.web.client.HttpHeadersProvider;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpHeaders;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
@EnableAdminServer
@EnableScheduling
public class SpringbootadminApplication {

	public Map<String, HttpHeaders> headersMap = new HashMap<>();

	public static void main(String[] args) {
		SpringApplication.run(SpringbootadminApplication.class, args);
	}

	@Bean
	public HttpHeadersProvider customHttpHeadersProvider() {
		return instance -> {
			headersMap.putIfAbsent(instance.getId().getValue(), new HttpHeaders());
			return headersMap.get(instance.getId().getValue());
		};
	}

}
