package com.epam.springbootadmin;

import de.codecentric.boot.admin.server.domain.entities.Instance;
import de.codecentric.boot.admin.server.domain.entities.InstanceRepository;
import de.codecentric.boot.admin.server.web.client.HttpHeadersProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import reactor.core.publisher.Flux;

import java.util.List;

@Component
public class ClientAuthenticator {

    @Autowired
    HttpHeadersProvider customHttpHeadersProvider;

    @Autowired
    InstanceRepository instanceRepository;

    @Scheduled(initialDelay = 0, fixedDelayString = "20000")
    public void setJSessionIDForAllApps() {
        Flux<Instance> instanceFlux = instanceRepository.findAll();
        instanceFlux.subscribe(this::setJSessionID);
    }

    private void setJSessionID(Instance instance) {
        ResponseEntity<String> response = logIntoApplication();
        List<String> cookies = response.getHeaders().get("Set-Cookie");
        String JSession = cookies.get(0).split(";")[0];
        System.out.println(JSession);
        customHttpHeadersProvider.getHeaders(instance).clear();
        customHttpHeadersProvider.getHeaders(instance).set("Cookie", JSession);
        System.out.println(customHttpHeadersProvider.getHeaders(instance).get("Cookie"));
    }

    private ResponseEntity<String> logIntoApplication() {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("username", "default@default.com");
        map.add("password", "password");

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);

        ResponseEntity<String> response = restTemplate.postForEntity("http://localhost:8080/login", request, String.class);

        System.out.println(response.getStatusCodeValue());
        return response;
    }
}
